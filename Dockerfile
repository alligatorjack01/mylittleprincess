FROM node:14-alpine
RUN npm update -g path-parse@1.0.7

WORKDIR /usr/src/app
COPY package.json .
RUN npm install
COPY . .

CMD [ "npm", "start" ]
